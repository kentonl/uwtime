# UWTime#

## Introduction ##

UWTime is a temporal semantic parser that detects and resolves time expressions in text. The system is described in:

>[Context-dependent Semantic Parsing for Time Expressions ](http://homes.cs.washington.edu/~kentonl/pub/ladz-acl.2014.pdf)

>[Kenton Lee](http://homes.cs.washington.edu/~kentonl), [Yoav Artzi](http://yoavartzi.com/), [Jesse Dodge](http://www.cs.cmu.edu/afs/cs/Web/People/jessed) and [Luke Zettlemoyer](http://homes.cs.washington.edu/~lsz)

>*In Proceedings of the Conference of the Association for Computational Linguistics (ACL), 2014*

This repository contains UWTime, along with the tools necessary to replicate the experimental results reported in the paper. Downstream applications using UWTime as a tool should use https://bitbucket.org/kentonl/uwtime-standalone instead.

UWTime was implemented using [The University of Washington Semantic Parsing Framework](https://bitbucket.org/yoavartzi/spf). A demo of UWTime can be found at http://lil.cs.washington.edu/uwtime.

For questions and inquiries, please contact [Kenton Lee](mailto:kentonl@cs.washington.edu).

## Authors ##

Developed and maintained by [Kenton Lee](http://homes.cs.washington.edu/~kentonl)

Contributors: [Yoav Artzi](http://yoavartzi.com/), [Jesse Dodge](http://www.cs.cmu.edu/afs/cs/Web/People/jessed), [Luke Zettlemoyer](http://homes.cs.washington.edu/~lsz)

## Documentation ##
Reproduce the main results of the paper by following the instructions below.

### Building ###
To compile UWTime, use: ```ant dist``` at the root directory. The output JAR file will be in the ```dist``` directory.

### Running the experiments ###
Choose one of the experimental configurations found in the ```configs``` directory. Run the experiment by providing the jar with a configuration file as the argument. For example, ```java -jar dist/uwtime-1.0.0.jar configs/tempeval/tempeval_test.yml```

### Evaluation Statistics ###
The results can be viewed in two ways:

1. UWTime computes its own evaluation statistics during the experiment, and the results are printed to standard output.

2. You can also use the official TempEval-3 evaluation toolkit to compute the statistics. The annotated TimeML files are dumped in the ```output``` directory. While in the ```evaluation_tools``` directory, run the evaluation script with the gold standard directory and the system output directory as arguments. For example, ```python TE3-evaluation.py ../data/tempeval_platinum/ ../output/tempeval_test/```. For the development results, use the ```corrections``` directory as the gold standard.

## License ##
UWTime - Copyright (C) 2014 Kenton Lee

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.