package edu.uw.cs.lil.uwtime.data;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.EnglishGrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.uw.cs.lil.tiny.data.collection.IDataCollection;
import edu.uw.cs.lil.uwtime.chunking.chunks.TemporalJointChunk;
import edu.uw.cs.lil.uwtime.learn.temporal.MentionResult;
import edu.uw.cs.utils.composites.Pair;

public class TemporalDocument implements Serializable, IDataCollection<TemporalSentence>{
    private static final long serialVersionUID = -3478876003961945902L;
    private String docID;
    private String bodyText;
    private String documentCreationText;
    
    private Map<Pair<TemporalSentence, Integer>, Pair<Integer, Integer>> tokenToChar;
    private Map<Integer, Pair<TemporalSentence, Integer>> startCharToToken, endCharToToken;

    private List<TemporalJointChunk> bodyMentions;
    private List<TemporalSentence> bodySentences;
    private TemporalJointChunk documentCreationMention;

    final private static String DOCUMENT_FORMAT = 
            "<?xml version=\"1.0\" ?>\n" + 
                    "<TimeML xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://timeml.org/timeMLdocs/TimeML_1.2.1.xsd\">\n" + 				
                    "<DOCID>%s</DOCID>\n" +
                    "<DCT><TIMEX3 tid=\"t0\" type=\"DATE\" value=\"%s\" temporalFunction=\"false\" functionInDocument=\"CREATION_TIME\">%s</TIMEX3></DCT>\n" + 
                    "<TEXT>%s</TEXT>" + 
                    "\n</TimeML>";

    public TemporalDocument() {
        this.bodyMentions = new LinkedList<TemporalJointChunk>();
        this.tokenToChar = new HashMap<Pair<TemporalSentence, Integer>, Pair<Integer, Integer>>();
    }

    public TemporalDocument(TemporalDocument other) {
        this.docID = other.docID;
        this.bodyText = other.bodyText;
        this.documentCreationText = other.documentCreationText;
        
        this.tokenToChar = other.tokenToChar;
        this.startCharToToken = other.startCharToToken;
        this.endCharToToken = other.endCharToToken;
        
        this.bodyMentions = other.bodyMentions;
        this.bodySentences = other.bodySentences;
        this.documentCreationMention = other.documentCreationMention;
    }

    public void setText(String text) {
        this.bodyText = text;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getDocID() {
        return docID;
    }

    public void insertMention(TemporalJointChunk chunk) {
        bodyMentions.add(chunk);
    }

    // For timeml with embedded mentions
    public void insertMention(String type, String value, String mod, int offset, int tid) {
        insertMention(new TemporalJointChunk(type, value, mod, offset, -1, tid));
    }

    // For clinical narratives with separate mentions
    public void insertMention(String type, String value, String mod, int startChar, int endChar, int tid) {
        insertMention(new TemporalJointChunk(type, value, mod, startChar, endChar, tid));
    }

    public void insertDCTMention(TemporalJointChunk chunk) {
        documentCreationMention = chunk;
    }

    public void insertDCTMention(String type, String value, String mod, int offset, int tid) {
        insertDCTMention(new TemporalJointChunk(type, value, mod, offset, -1, tid));
    }

    public void setLastMentionText(String text) {
        bodyMentions.get(bodyMentions.size() - 1).setCharEnd(text);
    }

    public void setDCTMentionText(String text) {
        documentCreationMention.setCharEnd(text);
    }

    public void setDCTText(String text) {
        documentCreationText = text;
    }

    private static String prefilterText(String s) {
        return s.replace("-", " ").replace("/", " ").replace("&", " ");
        //return s.replace("&", " ");
    }

    private static String postfilterText(String s) {
        return s.replace("\\/", "/");
    }

    private List<TemporalSentence> getPreprocessedSentences(String text, List<TemporalJointChunk> mentions, 
            StanfordCoreNLP pipeline, GrammaticalStructureFactory gsf) {
        Annotation a = new Annotation(text);
        pipeline.annotate(a);

        startCharToToken = new HashMap<Integer, Pair<TemporalSentence, Integer>>();
        endCharToToken = new HashMap<Integer, Pair<TemporalSentence, Integer>>();

        List<TemporalSentence> sentences = new LinkedList<TemporalSentence>();

        for(CoreMap sentence: a.get(SentencesAnnotation.class)) {
            TemporalSentence newSentence = new TemporalSentence(this, getDocumentCreationTime().getResult().getValue());
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                Pair<TemporalSentence, Integer> tokenIndex = Pair.of(newSentence, newSentence.getNumTokens());
                startCharToToken.put(token.beginPosition(), tokenIndex);
                endCharToToken.put(token.endPosition(), tokenIndex);
                tokenToChar.put(tokenIndex, Pair.of(token.beginPosition(), token.endPosition()));
                newSentence.insertToken(postfilterText(token.get(TextAnnotation.class)));
            }
            Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
            GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
            String dp = EnglishGrammaticalStructure.dependenciesToString(gs, gs.typedDependencies(false), tree, true, false);
            newSentence.saveDependencyParse(dp);
            sentences.add(newSentence);
        }

        for(TemporalJointChunk t : mentions)
            t.alignTokens(startCharToToken, endCharToToken);
        return sentences;
    }

    public void doPreprocessing(StanfordCoreNLP pipeline, GrammaticalStructureFactory gsf) {
        bodySentences = getPreprocessedSentences(documentCreationText, Collections.singletonList(documentCreationMention), pipeline, gsf);
        bodySentences.addAll(getPreprocessedSentences(prefilterText(bodyText), bodyMentions, pipeline, gsf));
    }


    public TemporalJointChunk getDocumentCreationTime() {
        return documentCreationMention;
    }
    
    public String getDocumentCreationText() {
    	return documentCreationText;
    }

    public List<TemporalSentence> getSentences() {
        return bodySentences;
    }

    public String toString() {
        return bodyText;
    }

    public Pair<Integer, Integer> getCharRange (TemporalSentence sentence, int i) {
        return tokenToChar.get(Pair.of(sentence, i));
    }
    
    public Map<Pair<TemporalSentence, Integer>, Pair<Integer, Integer>> getTokenToChar() {
        return tokenToChar;
    }
    
    public Pair<TemporalSentence, Integer> getTokenFromStartChar(int startChar) {
        return startCharToToken.get(startChar);
    }
    
    public Pair<TemporalSentence, Integer> getTokenFromEndChar(int endChar) {
        return endCharToToken.get(endChar);
    }
    
    @Override
    public Iterator<TemporalSentence> iterator() {
        return bodySentences.iterator();
    }

    @Override
    public int size() {
        return bodySentences.size();
    }

    public String annotatePredictions(List<Pair<TemporalSentence, TemporalJointChunk>> allPredictions) {
        char[] originalDocumentText = bodyText.toCharArray();
        Map<Integer, String> annotationInsertionMap = new HashMap<Integer, String>();
        int tidCount = 1;
        for (Pair<TemporalSentence, TemporalJointChunk> prediction : allPredictions) {
            Pair<Integer, Integer> startTokenSpan = tokenToChar.get(Pair.of(prediction.first(), prediction.second().getStart()));
            Pair<Integer, Integer> endTokenSpan = tokenToChar.get(Pair.of(prediction.first(), prediction.second().getEnd()));
            annotationInsertionMap.put(startTokenSpan.first(), prediction.second().getResult().beginAnnotation("t" + tidCount));
            annotationInsertionMap.put(endTokenSpan.second(), prediction.second().getResult().endAnnotation());
            tidCount++;
        }

        StringBuilder annotatedDocument = new StringBuilder();
        for (int i = 0 ; i < originalDocumentText.length ; i++) {
            if (annotationInsertionMap.containsKey(i))
                annotatedDocument.append(annotationInsertionMap.get(i));
            annotatedDocument.append(originalDocumentText[i]);
        }
        return annotate(annotatedDocument.toString());
    }

    public String annotate(String text) {
        return String.format(DOCUMENT_FORMAT, docID, getDocumentCreationTime().getResult().getValue(), getDocumentCreationTime().getPhrase(), text.replace("&",  "&amp;"));
    }

    public String getOriginalText() {
        return bodyText;
    }

    public String getOriginalText(int charStart, int charEnd) {
        return bodyText.substring(charStart, charEnd);
    }
    
    public TemporalDocument withoutDCT() {
        TemporalDocument newDoc = new TemporalDocument(this);
        newDoc.bodySentences.remove(documentCreationMention.getSentence());
        newDoc.bodyMentions.remove(documentCreationMention);
        return newDoc;
    }

    public String dump() {
        char[] originalDocumentText = bodyText.toCharArray();
        Map<Integer, List<String>> annotationInsertionMap = new HashMap<Integer, List<String>>();
        int tidCount = 1;
        for(TemporalSentence ts : this) {
            List<TemporalJointChunk> annotatedChunks = new LinkedList<TemporalJointChunk>();
            for(TemporalJointChunk goldChunk : ts.getLabel()) {	
                boolean hasOverlap = false;
                for(TemporalJointChunk possibleOverlap : annotatedChunks)
                    if (possibleOverlap.overlapsWith(goldChunk)) {
                        hasOverlap = true;
                        break;
                    }
                if (hasOverlap || goldChunk.getEnd() < goldChunk.getStart())
                    continue;
                MentionResult goldResult = goldChunk.getResult();
                String tid = "t" + (goldChunk.getTID() != -1 ? goldChunk.getTID() : tidCount);
                Pair<Integer, Integer> startTokenSpan = tokenToChar.get(Pair.of(ts, goldChunk.getStart()));
                Pair<Integer, Integer> endTokenSpan = tokenToChar.get(Pair.of(ts, goldChunk.getEnd()));

                if (!annotationInsertionMap.containsKey(startTokenSpan.first()))
                    annotationInsertionMap.put(startTokenSpan.first(), new LinkedList<String>()); 
                annotationInsertionMap.get(startTokenSpan.first()).add(goldResult.beginAnnotation(tid));

                if (!annotationInsertionMap.containsKey(endTokenSpan.second()))
                    annotationInsertionMap.put(endTokenSpan.second(), new LinkedList<String>());
                annotationInsertionMap.get(endTokenSpan.second()).add(goldResult.endAnnotation());
                tidCount++;
                annotatedChunks.add(goldChunk);
            }
        }
        StringBuilder annotatedDocument = new StringBuilder();
        for (int i = 0 ; i < originalDocumentText.length ; i++) {
            if (annotationInsertionMap.containsKey(i))
                for(String s : annotationInsertionMap.get(i))
                    annotatedDocument.append(s);
            annotatedDocument.append(originalDocumentText[i]);
        }
        return annotate(annotatedDocument.toString());
    }
}
