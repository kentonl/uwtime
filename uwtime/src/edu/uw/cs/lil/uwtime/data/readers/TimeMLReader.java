package edu.uw.cs.lil.uwtime.data.readers;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import edu.uw.cs.lil.uwtime.data.TemporalDataset;
import edu.uw.cs.lil.uwtime.data.TemporalDocument;
import edu.uw.cs.lil.uwtime.utils.TemporalLog;


public class TimeMLReader extends AbstractTemporalReader {
	private String currentText;
	private String currentMention;
	private String currentDocID;
	private String currentDCT;
	private boolean isReadingText;
	private boolean isReadingMention;
	private boolean isReadingDocID;
	private boolean isReadingDCT;
	private TemporalDocument currentDocument;

	@Override
	public TemporalDataset getDataset(String datasetRoot, String datasetName) throws IOException, SAXException, ParserConfigurationException  {
		new File(SERIALIZED_DIR).mkdirs();
		File serializedFile = new File(SERIALIZED_DIR + datasetName + ".ser");
		TemporalLog.println("progress", "Looking for serialized data at " + serializedFile);
		if(serializedFile.exists()) {
			TemporalLog.println("progress","Serialized data found. Deserializing data...");
			try {
				return TemporalDataset.deserialize(serializedFile.getPath());
			}
			catch (ClassNotFoundException e) {
			}
		}
		if (!serializedFile.exists())
			TemporalLog.println("progress", "Serialized data unavailable.");
		else
			TemporalLog.println("progress", "Serialized data invalid.");

		initLibraries();

		TemporalLog.println("progress","Reading and dependency parsing data...");

		long startTime = System.nanoTime();
		
		File xmlDir = new File(datasetRoot + datasetName);
		File[] xmlFiles = xmlDir.listFiles();
		TemporalLog.printf("progress","Reading %d files from %s\n", xmlFiles.length, datasetName);
		int count = 0;
		TemporalDataset dataset = new TemporalDataset(datasetName);
		for(File f: xmlFiles) {
			if (!f.getName().endsWith(".tml")) {
				TemporalLog.println("error", "Found " + f.getName() + ", which does not belong here!");
				continue;
			}
			count ++;
			TemporalLog.printf("progress","[%d/%d] Parsing %s\n", count, xmlFiles.length, f.getName());
			currentDocument = new TemporalDocument();
			sp.parse(xmlDir.getPath() + "/" + f.getName(), this);
			currentDocument.doPreprocessing(pipeline, gsf);
			dataset.addDocument(currentDocument);
		}

		long endTime = System.nanoTime();
		dataset.serialize(serializedFile.getPath());
		TemporalLog.printf("progress","%d sentences parsed and serialized (%.2f seconds)\n", dataset.size(), (endTime - startTime)*1.0e-9);
		return dataset;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equals("TIMEX3")) {
			String value = attributes.getValue("value");
			String type = attributes.getValue("type");
			String mod = attributes.getValue("mod");
			int tid = Integer.parseInt(attributes.getValue("tid").replaceFirst("t", ""));

			if (isReadingText)
				currentDocument.insertMention(type, value, mod, currentText.length(), tid);
			if (isReadingDCT)
				currentDocument.insertDCTMention(type, value, mod, currentDCT.length(), tid);
			isReadingMention = true;
			currentMention = "";
		}
		else if (qName.equals("TEXT")) {
			isReadingText = true;
			currentText = "";
		}
		else if (qName.equals("DCT")) {
			isReadingDCT = true;
			currentDCT = "";
		}
		else if (qName.equals("DOCID")){
			isReadingDocID = true;
			currentDocID = "";
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (qName.equals("TIMEX3")) {
			if (isReadingMention) {
				if (isReadingText)
					currentDocument.setLastMentionText(currentMention);
				if (isReadingDCT)
					currentDocument.setDCTMentionText(currentMention);
			}
			isReadingMention = false;
		}
		else if (qName.equals("TEXT") ) {
			currentDocument.setText(currentText);
			isReadingText = false;
		}
		else if (qName.equals("DCT") ) {
			currentDocument.setDCTText(currentDCT);
			isReadingDCT = false;
		}
		else if (qName.equals("DOCID")){
			currentDocument.setDocID(currentDocID);
			isReadingDocID = false;
		}
	}

	@Override
	public void characters(char[] buffer, int start, int length) {
		String text = new String(buffer, start, length);
		if (isReadingText)
			currentText += text;
		if (isReadingMention)
			currentMention += text;
		if (isReadingDocID)
			currentDocID += text;
		if (isReadingDCT) 
			currentDCT += text;
	}
}
